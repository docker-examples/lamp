FROM php:7.4-apache

RUN a2enmod rewrite

RUN docker-php-ext-install mysqli

WORKDIR /var/www/html

RUN usermod -u 1000 www-data
RUN usermod -a -G users www-data

RUN chown -R www-data:www-data /var/www