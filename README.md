# LAMP #
* Linux
* Apache
* Mysql 8.0 
* PHP 7.4 + Phpmyadmin

Neste exemplo, utilizamos um container com versão linux completa, contendo o Apache como webserver. 
Note que a pasta /src contém um exemplo de código em PHP (hello world) e outra pasta para armazenar arquivos mysql.

### Comandos ###
docker-compose up

http://localhost
http://localhost:8000 -> phpmysadmin
